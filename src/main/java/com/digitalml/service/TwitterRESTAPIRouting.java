package com.digitalml.service;

import static spark.Spark.*;
import spark.*;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import com.mashape.unirest.request.body.MultipartBody;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Context;
import com.github.jknack.handlebars.Context.Builder;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.context.JavaBeanValueResolver;
import com.github.jknack.handlebars.context.MapValueResolver;

import static net.logstash.logback.argument.StructuredArguments.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class TwitterRESTAPIRouting {

    private static final Logger logger = LoggerFactory.getLogger("twitterrestapi:1.1");

    private static final String cacheURL = "http://infocache-test:9200/twitterrestapi-1.1";
    
    private static final String resourceName = "twitterrestapi";

    public static void main(String[] args) {
   
   		// Initialise index on startup
		Unirest.put(cacheURL).asStringAsync();
   
        port(4567);
    
        get("/ping", (req, res) -> {
            return "pong";
        });
        
        get("/halt", (request, response) -> {
			stop();
			response.status(202);
			return "";
		});
		
        // Handle timings
        
        Map<Object, Long> timings = new ConcurrentHashMap<>();
        
        before(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.put(request, System.nanoTime());
        	}
        });
        
        after(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		long start = timings.remove(request);
        		long end =  System.nanoTime();
        		logger.info("log message {} {} {} {} ns", value("apiname", "twitterrestapi"), value("apiversion", "1.1"), value("apipath", request.pathInfo()), value("response-timing", (end-start)));
        	}
        });
        
        afterAfter(new Filter() {
        	@Override
        	public void handle(Request request, Response response) throws Exception {
        		timings.remove(request);
        	}
        });

        get("/lists/subscribers/destroy", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/list/members/create", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/lists/update", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/geo/reverse_geoncode", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/saved_searches/list", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/help/privacy", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/users/report_spam", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/help/configuration", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/list/members/show", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/statuses/show/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/" + req.params("id") + "/_source").asString();
			
			res.status(cacheResponse.getStatus());
            
            return cacheResponse.getBody();
        });
        
        get("/geo/places", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/blocks/create", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/direct_messages/sent", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/lists/subscribers/show", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/account/update_delivery_device", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/statuses/oembed", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/geo/id/:place_id", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/users/show", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/friendships/destroy", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/direct_messages", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/saved_searches/show/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/" + req.params("id") + "/_source").asString();
			
			res.status(cacheResponse.getStatus());
            
            return cacheResponse.getBody();
        });
        
        get("/lists/show", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/friendships/update", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/account/update_profile_image", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/statuses/destroy/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/help/languages", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/users/contributors", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/trends/available", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/blocks/ids", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/geo/similar_places", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/friendships/outgoing", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/application/rate_limit_status", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/lists/subscribers", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/list/members/destroy_all", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/lists/members/create_all", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/friendships/incoming", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/account/settings", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/account/settings", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/trends/closest", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/users/contributees", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/lists/list", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/account/update_profile_background_image", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/account/update_profile", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/help/tos", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/saved_searches/create", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/statuses/retweets/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/" + req.params("id") + "/_source").asString();
			
			res.status(cacheResponse.getStatus());
            
            return cacheResponse.getBody();
        });
        
        post("/statuses/retweets/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/direct_messages/destroy", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/blocks/list", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/statuses/update", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/friendships/show", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/lists/statuses", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/list/members", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/followers/ids", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/account/update_profile_colors", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/statuses/user_timeline", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/friendships/create", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/users/lookup", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/trends/place", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/blocks/destroy", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/users/search", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/statuses/home_timeline", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/search/tweets", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/lists/subscribers/create", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        post("/direct_messages/new", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/direct_messages/show", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/statuses/mentions_timeline", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/lists/memberships", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/lists/destroy", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/friends/ids", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/lists/create", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/lists/members/destroy", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        post("/saved_searches/destroy/:id", (req, res) -> {

            HttpResponse<String> cacheResponse = Unirest.post(cacheURL + "/" + resourceName + "/" + req.params("id")).body(req.body()).asString();

			res.status(cacheResponse.getStatus());
			if (cacheResponse.getStatus() < 200 || cacheResponse.getStatus() > 299)
				return null;

			return cacheResponse.getBody();
        });
        
        get("/lists/subscriptions", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
        get("/geo/search", (req, res) -> {

            
            long max = req.queryParams("page") == null ? 10 : Long.parseLong(req.queryParams("page"));
            long offset = req.queryParams("offset") == null  ? 0 : Long.parseLong(req.queryParams("offset"));
            String sort = req.queryParams("sort") == null ? "" : req.queryParams("sort");
            
            HttpResponse<String> cacheResponse = Unirest.get(cacheURL + "/" + resourceName + "/_search?filter_path=hits.hits._source&from=" + offset + "&size=" + max + "&sort=" + sort).asString();
            String result = cacheResponse.getBody();
            
            res.status(cacheResponse.getStatus());
            
			if (result.length() <= 2)
				return "[]";

			result = result.substring(17, result.length() - 4).replace("},{\"_source\":", ",").replace("{\"_source\":", "");

			return "[" + result + "]";
        });
        
    }
}